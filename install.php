<?php require_once("common.php"); ?>

<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);


	if(isset($_GET['id']) && !empty($_GET['id']) && ctype_digit($_GET['id'])) {
		$id = $_GET['id'];

	}
	else {
		header("Location:index.php");
		die();
	}

	
	$stmt = $db->prepare("SELECT *  FROM extensions WHERE id = :id");
	$stmt->bindValue(":id", $id, PDO::PARAM_INT);
	$stmt->execute();
	$extension = $stmt->fetch(PDO::FETCH_ASSOC);

	echo "ok";

	if(!$extension)
	{die("a");

		header("Location:index.php");
		die();
	}

	// inc the install counter
	$stmt = $db->prepare("UPDATE extensions SET installs = installs+1 WHERE id = :id");
	$stmt->bindValue(":id", $id, PDO::PARAM_INT);
	$stmt->execute();

	header("Location:https://addons.mozilla.org/firefox/downloads/latest/".$extension["original_id"]."/addon-".$extension["original_id"]."-latest.xpi");
	die();

?>
