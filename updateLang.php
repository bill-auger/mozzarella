<?php
/*
 *  Copyright (C) 2022,2023 Joey Allard
 *  Copyright (C) 2023 Ruben Rodriguez <ruben@gnu.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

	require_once("common.php");
	if(isset($_POST["lang"]) && !empty($_POST["lang"])
	&& isset($_POST["next"]) && !empty($_POST["next"])) {
		// check if the locale exists, set the session, and redirect to "next"
		$stmt = $db->prepare("SELECT * FROM extension_locale WHERE locale = :locale");
		$stmt->bindValue(":locale", $_POST["lang"]);
		$stmt->execute();
		if($stmt->fetch()) {
			$_SESSION['lang'] = $_POST["lang"];
		}

		header("Location:http://".$_POST["next"]);
		die();
	}


?>