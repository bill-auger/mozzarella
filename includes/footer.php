<?php
/*
 *  Copyright (C) 2022,2023 Joey Allard
 *  Copyright (C) 2023 Ruben Rodriguez <ruben@gnu.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
?>
<footer>
	<div>
		Please report any bugs or incorrect information regarding the add-ons to the
		<a href="https://gitlab.trisquel.org/joeall/mozzarella/-/issues">Mozzarella bug tracker</a>.
	</div>

	<a href="https://github.com/spdx/license-list-data/blob/master/json/licenses.json">Licenses</a>
	<?php 
		// get the locales
		$stmt = $db->prepare("SELECT distinct(locale) FROM extension_locale ORDER BY locale");
		$stmt->execute();
		$locales = $stmt->fetchAll(PDO::FETCH_ASSOC) ?? [];
	?>

	<form action="updateLang.php" method="POST">
		<select name="lang">
			<?php
				foreach($locales as $locale) {
					?>
					<option <?php if($locale["locale"] == $_SESSION["lang"]) echo "selected"; ?>><?=$locale["locale"]?></option>
					<?php
				}
			?>
		</select>
		<input type="hidden" name="next" value="<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>">
		<button type="submit">Apply</button>
	</form>
</footer>
