<?php
/*
 *  Copyright (C) 2022,2023 Joey Allard
 *  Copyright (C) 2023 Ruben Rodriguez <ruben@gnu.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

	require_once("common.php");

	// ini_set('display_errors', 1);
	// ini_set('display_startup_errors', 1);
	// error_reporting(E_ALL);

	if(isset($_GET['q']) && !empty($_GET['q'])) {
		$q = $_GET['q'];
	}
	else {
		header("Location:index.php");
		die();
	}

	$page = 1;
	if(isset($_GET['page']) && !empty($_GET['page']) && ctype_digit($_GET['page'])) {
		$page = $_GET['page'];
	}
	$offset = $page * PER_PAGE - PER_PAGE;

	$stmt = $db->prepare("SELECT * FROM (SELECT * FROM extension_locale
		INNER JOIN extensions USING (ext_id)
		WHERE ext_id IN (
			SELECT ext_id FROM extension_locale 
			WHERE (name LIKE :q OR description LIKE :q OR summary LIKE :q)
			AND (locale = :locale or locale = 'en-US'))
		AND (locale = :locale or locale = 'en-US')
		GROUP by ext_id,locale
		ORDER BY average_daily_users DESC, FIELD(locale, :locale, 'en-US') ASC)
		AS c GROUP BY ext_id ORDER BY average_daily_users DESC");
	$stmt->bindValue(":locale", $_SESSION['lang']);
	$stmt->bindValue(":q", '%'.$_GET['q'].'%');
	$stmt->execute();

	$extensions = $stmt->fetchAll(PDO::FETCH_ASSOC) ?? [];
	if (! $extensions){
		header("Location:index.php");
		die();
	}

/* Handled by SQL

	// at this point, contains every result for the searched terms.
	// need to filter the results based on the language, by this order: user set language/ (or default session language), any en- language, default extension language, or any language if none is preferred

	// user locale exists in $_SESSION (see common.php), defaulted to "en-US"
	// print_r($extensions);
	$extensionsFiltered = [];
	// $_SESSION['lang'] = "fr";
	// index on the $extensions array
	$i=0;
	do {
		// if the last extension added to the filtered array is the same as the current one, only override if his language has more "weight"
		// 1. $_SESSION language
		// 2. language that starts with "en-"
		// 3. extension default language
		// 4. any language (there is no preferenceby the dev (defaultLocale) ) (do not override it)
		if(count($extensionsFiltered) > 0 && $extensionsFiltered[count($extensionsFiltered)-1]["id"] == $extensions[$i]["id"]) {
			// extension already exists, replace if the language is different than the session lang
			$existingLocale = $extensionsFiltered[count($extensionsFiltered)-1]["locale"];
			$currentLocale = $extensions[$i]["locale"];

			
			if(
				$extensions[$i]["isDefaultLocale"] && !strpos($existingLocale, "en-") !== false && !$existingLocale == $_SESSION["lang"]) {
				// override by the default locale language only if the 2. is not verified
				// if the extension has the default lang, replace
				$extensionsFiltered[count($extensionsFiltered)-1] = $extensions[$i];
			}
			else if(strpos($currentLocale, "en-") !== false &&  $existingLocale != $_SESSION['lang']){
				// but if extension locale starts with en-, override it
				// replace if language starts with "en-"
				$extensionsFiltered[count($extensionsFiltered)-1] = $extensions[$i];
			}
			else if($currentLocale == $_SESSION['lang']) {
				// if extension has user session lang, override it
				$extensionsFiltered[count($extensionsFiltered)-1] = $extensions[$i];
				// echo $currentLocale;
			}
			
		}
		else {
			// extension not added yet, add it
			$extensionsFiltered[] = $extensions[$i];
		}
		$i++;
	} while($i < count($extensions));
	 // && count($extensionsFiltered) < 30);
	$count = count($extensionsFiltered);

	// print_r(count($extensionsFiltered));

	// $extensions = $extensionsFiltered;
	$extensions = array_slice($extensionsFiltered, $offset, PER_PAGE);

	// calculate the max page
	// ceil: ex. 100 results / 30 elem per page: 3.33 => need 4 pages
	$maxPage = ceil($count/PER_PAGE);
	if($page > 1 && $page > $maxPage) {
		// back to page 1
		header("Location:search.php?q=".$_GET["q"]);
		die();
	}

 */

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<link rel="stylesheet" type="text/css" href="fontawesome-free-6.4.0-web/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="main.css">
	<style type="text/css">
		#container {
			display: flex;
			max-width: 1366px;
			margin: auto;

		}
		#left {
			background: orange;
			background: white;
			margin-right: 10px;
			padding: 10px;
			display: none;
		}
		#main {
			background: white;
			flex: 1;
			padding: 10px;
			display: flex;
			flex-direction: column;
		}
		
		

		@media screen and (max-width: 1000px)
		{
			#container {
				flex-direction: column;
			}

			background: red;
		}
		#left {
			background: orange;

		}
	</style>
</head>
<body>
	<?php include("includes/header.php"); ?>

	<h1><?=$count?> search results for "<?=htmlspecialchars($_GET["q"])?>"</h1>

	<div id="container">
		<div id="left">
			a
		</div>
		<div id="main">
			<?php 
				if($extensions) {
					foreach($extensions as $e)
					{
						?>
						<div class="extension">
							<img src="images/icons/<?=$e["ext_id"]?>.png">
							<div class="extensionRight">
								<a href="extension.php?id=<?=$e["ext_id"]?>"><?=$e["name"]?></a>
								<p><?=nl2br(strip_tags($e["summary"] ))?></p>
							</div>
						</div>
						<?php
					}
				}

				?>
				<div id="pagination">
					
					<?php

						if($page > 1)
						{
							?>
							<a class="pagination" href="search.php?q=<?=$_GET['q']?>&page=<?=$page-1?>">Previous</a>
							<?php
						}
						if($page < ceil($count / PER_PAGE))
						{
							?>
							<a class="pagination" href="search.php?q=<?=$_GET['q']?>&page=<?=$page+1?>">Next</a>
							<?php
						}

					?>

				</div>
				



		</div>
	</div>
	<?php include("includes/footer.php"); ?>
	
	
</body>
</html>