<?php
/*
 *  Copyright (C) 2022,2023 Joey Allard
 *  Copyright (C) 2023 Ruben Rodriguez <ruben@gnu.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

	require_once("common.php");

	// ini_set('display_errors', 1);
	// ini_set('display_startup_errors', 1);
	// error_reporting(E_ALL);

	// print_r($db);
	// get all categories
	$stmt = $db->prepare("SELECT DISTINCT categories.* FROM categories inner join ext_cat on categories.cat_id = ext_cat.cat_id ORDER BY cat_id");
	$stmt->execute();
	$categories = $stmt->fetchAll(PDO::FETCH_ASSOC);

	// print_r($categories);

	$browse = "popular";
	if(isset($_GET["browse"]) && !empty($_GET["browse"]) && in_array($_GET["browse"], ["popular", "recent"])) {
		$browse = $_GET["browse"];
	}

	$page = 1;
	if(isset($_GET["page"]) && !empty($_GET["page"]) && ctype_digit($_GET['page'])) {
		$page = $_GET['page'];
	}
	// 30 per page

	// count the max allowed page
	$count = $db->query("SELECT count(*) c FROM extensions")->fetch(PDO::FETCH_ASSOC)["c"];

	$maxPage = ceil($count / PER_PAGE);
	
	if($maxPage && $page > $maxPage) {
		// exceeded max page
		// back to first page
		header("Location:index.php");
		die();
	}

	$offset = $page * PER_PAGE - PER_PAGE;

	// get the lastly added extensions, with his default locale name if has one (or the first one)
	// + pagination
	// display 30 per page: 30 can be displayed in 6,5,3,2 or 1 column(s) perfectly


	// get the latest, apply the page to this list if browsing the recent extensions, or apply to the most popular
	$stmt = $db->prepare("SELECT * FROM (
	SELECT * FROM extensions
	INNER JOIN extension_locale USING (ext_id)
	WHERE locale = :locale or locale = 'en-US'
	GROUP BY ext_id, locale
	ORDER BY ext_id DESC, FIELD(locale, :locale, 'en-US') ASC
	LIMIT :l OFFSET :o
	) AS c GROUP BY ext_id ORDER BY ext_id DESC");
	$stmt->bindValue(":l", PER_PAGE, PDO::PARAM_INT);
	$stmt->bindValue(":o", $offset, PDO::PARAM_INT);
	$stmt->bindValue(":locale", $_SESSION['lang']);
	$stmt->execute();
	$latest = $stmt->fetchAll(PDO::FETCH_ASSOC) ?? [];

	// get popular (most installed (from this site))

	$stmt = $db->prepare("SELECT * FROM (
	SELECT * FROM extensions
	INNER JOIN extension_locale USING (ext_id)
	WHERE locale = :locale or locale = 'en-US'
	GROUP BY ext_id, locale
	ORDER BY promoted DESC, average_daily_users DESC, FIELD(locale, :locale, 'en-US') ASC
	LIMIT :l OFFSET :o
	) AS c GROUP BY ext_id ORDER BY promoted DESC, average_daily_users DESC");
	$stmt->bindValue(":l", PER_PAGE, PDO::PARAM_INT);
	$stmt->bindValue(":o", $offset, PDO::PARAM_INT);
	$stmt->bindValue(":locale", $_SESSION['lang']);
	$stmt->execute();
	$popular = $stmt->fetchAll(PDO::FETCH_ASSOC) ?? [];

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<link rel="stylesheet" type="text/css" href="fontawesome-free-6.4.0-web/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="main.css">
	
</head>
<body>
	<?php include("includes/header.php"); ?>
	<h1 class="subtitle">Free extensions for Firefox-based browsers</h1>
	<?php 
		if($categories) {

			?>
				<div id="categories" class="fluid">
			<?php

			foreach($categories as $cat)
			{
				?>
					<a href="category.php?id=<?=$cat["cat_id"]?>"><?=$cat["display_en"]?></a>
				<?php
			}

			?>
				</div>
			<?php

		}
			

	?>

	<div class="fluid" id="popularTitle">
		<div id="popularTitleLeft">
			<h2>Popular</h2>
		</div>

		<div class="paginationContainer">
			<?php 
				if($page > 1) {
					?>
			<a href="index.php?page=<?=$page-1?>&browse=popular" class="pagination">prev</a>

					<?php
				}
				if($page < $maxPage) {
					?>
			<a href="index.php?page=<?=$page+1?>&browse=popular" class="pagination">next</a>
					<?php
				}
			?>
		</div>
	</div>
	<div id="popular" class="fluid">
		<?php 
			foreach($popular as $l) {
				?>
				<a href="extension.php?id=<?=$l["ext_id"]?>" class="latest">
					<img src="images/icons/<?=$l["ext_id"]?>.png">
					<p><?=$l["name"]	?></p>
					<small><?=strip_tags($l["summary"])?></small>
				</a>
				<?php
			}
		?>
	</div>
	

	<div class="fluid" id="recentTitle">
		
		<h2>Recent</h2>
			<div class="paginationContainer">
			<?php 
				if($page > 1) {
					?>
			<a href="index.php?page=<?=$page-1?>&browse=recent#recentTitle" class="pagination">prev</a>

					<?php
				}
				if($page < $maxPage) {
					?>
			<a href="index.php?page=<?=$page+1?>&browse=recent#recentTitle" class="pagination">next</a>
					<?php
				}
			?>
		</div>
	</div>
	<div id="latest" class="fluid">
		<?php 
			foreach($latest as $l) {
				?>
				<a href="extension.php?id=<?=$l["ext_id"]?>" class="latest">
					<img src="images/icons/<?=$l["ext_id"]?>.png">
					<p><?=$l["name"]	?></p>
					<small><?=strip_tags($l["summary"])?></small>
				</a>
				<?php
			}
		?>
	</div>
	



	<?php include("includes/footer.php"); ?>

</body>
</html>